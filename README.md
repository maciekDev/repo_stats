# Repo Stats
Repo Stats provides simple REST API for comparing two repositories stored on GitHub. It offers two methods, one for single repository stats, second for comparing repos.

## Framework used
Built with [Symfony 4](https://symfony.com).

## Installation
Clone the repository to your location and start a server of your choice pointing to /public/index.php. If you plan to use it only locally you may want to use [Symfony Local Web Server](https://symfony.com/doc/current/setup/symfony_server.html).

## How to use
To be able to obtain and compare stats you must know :owner/:repo identifier of a given repository (e.g. symfony/symfony). It is always present in the URL of the repository on GitHub.
Both methods should be called by GET requests. Responses are formated as JSON.
### Methods
`GET /single-stats/:owner:/:repo`

This method will give you some basic stats of a given repository. Their names are self-explanatory.
Example response for request `/single-stats/hashicorp/vault`:
```json
{
"repo_name":"hashicorp\/vault",
"no_of_forks":1960,
"no_of_stars":13078,
"no_of_watchers":526,
"latest_release_date":null,
"no_of_open_pull_requests":96,
"no_of_closed_pull_requests":3924,
"last_year_commits_per_week":42.6
}
```


`GET /compare-repos?repo1=:owner/:repo&repo2=:owner/:repo`

This method will give you the same stats as the former one but for two repositories. Additionaly it offers minor summary in comparison section.
As you can see the method call has two query params: `repo1` and `repo2`. They are obligatory. If you omit one or two of them you will get "empty parameter" message.
Example response for request `/compare-repos?repo1=hashicorp/vault&repo2=probcomp/Gen`:
```json
{
"repository_statistics":
	[
		{
			"repo_name":"hashicorp\/vault",
			"no_of_forks":1960,
			"no_of_stars":13079,
			"no_of_watchers":526,
			"latest_release_date":null,
			"no_of_open_pull_requests":96,
			"no_of_closed_pull_requests":3924,
			"last_year_commits_per_week":42.6
		},
		{"
			repo_name":"probcomp\/Gen",
			"no_of_forks":32,
			"no_of_stars":472,
			"no_of_watchers":31,
			"latest_release_date":null,
			"no_of_open_pull_requests":1,
			"no_of_closed_pull_requests":42,
			"last_year_commits_per_week":10.5
		}
	],
"comparison":
	{
		"forks":"hashicorp\/vault has 61.3 more forks than probcomp\/Gen.",
		"latest_release":"hashicorp\/vault HAS NOT GOT a release within last month. probcomp\/Gen HAS NOT GOT a release within last month.",
		"commits":"During last year commits for hashicorp\/vault were 4.1 times more frequent than for probcomp\/Gen."
	}
}

```


Both methods will give you "Not found" exception if you enter non-existing repository.