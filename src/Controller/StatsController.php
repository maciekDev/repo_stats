<?php

namespace App\Controller;

use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class StatsController extends AbstractController
{
	/**
	 * @Route("/single-stats/{ownerAndRepo}", name="single-stats", requirements={"ownerAndRepo"=".+"}, methods={"GET"})
	 * @param string $ownerAndRepo
	 * @param bool $apiResponse
	 * @return type
	 * @throws NotFoundHttpException
	 */
	public function getSingleRepoStats(string $ownerAndRepo, bool $apiResponse = true) {
		$ghUser = $this->getParameter('app.github_user');
		$ghPass = $this->getParameter('app.github_pass');
		$ghUrl = $this->getParameter('app.github_url');
		$httpClient = HttpClient::create(['auth_basic' => [$ghUser, $ghPass]]);
		$repoResponse = $httpClient->request('GET',	$ghUrl.'/repos/'.$ownerAndRepo);
		
		$repoStatusCode = $repoResponse->getStatusCode();
		
		if ($repoStatusCode === 404) {
			if ($apiResponse) {
				return $this->json('There is no '.$ownerAndRepo.' repository on GitHub.', 404);
			} else {
				throw new NotFoundHttpException('There is no '.$ownerAndRepo.' repository on GitHub.');
			}	
		} elseif ($repoStatusCode !== 200) {
			if ($apiResponse) {
				return $this->json('Something went wrong on GitHub server.', 500);
			} else {
				return null;
			}
			
		}

		$repoContentArr = $repoResponse->toArray();
		$repoStats['repo_name'] = $ownerAndRepo;
		$repoStats['no_of_forks'] = $repoContentArr['forks_count'];
		$repoStats['no_of_stars'] = $repoContentArr['stargazers_count'];
		$repoStats['no_of_watchers'] = $repoContentArr['subscribers_count']; //watchers <=> subscribers
		
		$releaseResponse = $httpClient->request('GET', $ghUrl.'/repos/'.$ownerAndRepo.'/releases/latest');
		$releaseStatusCode = $releaseResponse->getStatusCode();
		
		if ($releaseStatusCode === 404) {
			$repoStats['latest_release_date'] = null;
		} elseif ($releaseStatusCode !== 200) {
			return $this->json('Something went wrong on GitHub server.', 500);
		} else {
			$releaseContentArr = $releaseResponse->toArray();
			$repoStats['latest_release_date'] = $releaseContentArr['published_at'];
		}

		$noOfOpenPulls = $this->getPullRequestsCount($ownerAndRepo, 'open');
		$noOfClosedPulls = $this->getPullRequestsCount($ownerAndRepo, 'closed');
		
		$repoStats['no_of_open_pull_requests'] = $noOfOpenPulls?$noOfOpenPulls:'No data.';
		$repoStats['no_of_closed_pull_requests'] = $noOfClosedPulls?$noOfClosedPulls:'No data.';
		
		$avgCommitCount = $this->getAverageWeeklyCommitCount($ownerAndRepo);
		$repoStats['last_year_commits_per_week'] = $avgCommitCount;

		if ($apiResponse) {
			return $this->json($repoStats);
		} else {
			return $repoStats;
		}
	}
	/**
	 * @Route("/compare-repos", name="compare-repos", methods={"GET"})
	 * @param Request $request
	 * @return type
	 */
	public function compareRepos(Request $request) {
		$ghUser = $this->getParameter('app.github_user');
		$ghPass = $this->getParameter('app.github_pass');
		$ghUrl = $this->getParameter('app.github_url');
		$repo1 = $request->query->get('repo1');
		if (!$repo1) {
			return $this->json('Please, specify the name (:owner/:name) of the first repository.');
		}
		$repo2 = $request->query->get('repo2');
		if (!$repo2) {
			return $this->json('Please, specify the name (:owner/:name) of the second repository.');
		}
		
		try {
			$repo1Stats = $this->getSingleRepoStats($repo1, false);
		} catch (NotFoundHttpException $ex) {
			return $this->json($ex->getMessage(), 404);
		}
		try {
			$repo2Stats = $this->getSingleRepoStats($repo2, false);
		} catch (NotFoundHttpException $ex) {
			return $this->json($ex->getMessage(), 404);
		}
		
		$comparison = [];
		
		$statsAndComparison['repository_statistics'] = [$repo1Stats, $repo2Stats];
		$statsAndComparison['comparison'] = $this->prepareStatsMessages($repo1Stats, $repo2Stats);
		
		return $this->json($statsAndComparison);
	}
	
	/**
	 * 
	 * @param array $repo1Stats
	 * @param array $repo2Stats
	 * @return array
	 */
	private function prepareStatsMessages(array $repo1Stats, array $repo2Stats): array {
		if ($repo1Stats['no_of_forks'] > 0 && $repo2Stats['no_of_forks'] > 0) {
			$forkRate = $repo1Stats['no_of_forks']/$repo2Stats['no_of_forks'];
			if ($forkRate >= 1) {
				$statsMsgs['forks'] = $repo1Stats['repo_name'].' has '.round($forkRate, 1).' more forks than '.$repo2Stats['repo_name'].'.';
			} else {
				$statsMsgs['forks'] = $repo2Stats['repo_name'].' has '.round(1/$forkRate, 1).' more forks than '.$repo1Stats['repo_name'].'.';
			} 
		} else {
			$statsMsgs['forks'] = 'No comparison info on forks (probably no forks for one of the repositories).';
		}
		
		$repo1DT = new DateTime($repo1Stats['latest_release_date']);
		$repo2DT = new DateTime($repo2Stats['latest_release_date']);
		
		$monthAgoDT = new DateTime(date("y-m-d",strtotime("-1 month")));

		$interval1 = $monthAgoDT->diff($repo1DT);
		if ($interval1->format('%m') == 0) {
			$statsMsgs['latest_release'] = $repo1Stats['repo_name'].' HAS GOT a release within last month.';
		} else {
			$statsMsgs['latest_release'] = $repo1Stats['repo_name'].' HAS NOT GOT a release within last month.';
		}
		$interval2 = $monthAgoDT->diff($repo2DT);
		if ($interval2->format('%m') == 0) {
			$statsMsgs['latest_release'] .= ' '.$repo2Stats['repo_name'].' HAS GOT a release within last month.';
		} else {
			$statsMsgs['latest_release'] .= ' '.$repo2Stats['repo_name'].' HAS NOT GOT a release within last month.';
		}

		if ($repo1Stats['last_year_commits_per_week'] > 0 && $repo2Stats['last_year_commits_per_week'] > 0) {
			$commitsRate = $repo1Stats['last_year_commits_per_week']/$repo2Stats['last_year_commits_per_week'];
			if ($commitsRate >= 1) {
				$statsMsgs['commits'] = 'During last year commits for '.$repo1Stats['repo_name'].' were '.round($commitsRate,1).' times more frequent than for '.$repo2Stats['repo_name'].'.';
			} else {
				$statsMsgs['commits'] = 'During last year commits for '.$repo2Stats['repo_name'].' were '.round(1/$commitsRate,1).' times more frequent than for '.$repo1Stats['repo_name'].'.';
			} 
		} else {
			$statsMsgs['commits'] = 'No comparison info on commits (probably no commits for one of the repositories).';
		}
		
		return $statsMsgs;
		
	}
	/**
	 * 
	 * @param string $ownerAndRepo
	 * @param string $state
	 * @return int
	 */
	private function getPullRequestsCount(string $ownerAndRepo, string $state): int {
		$ghUser = $this->getParameter('app.github_user');
		$ghPass = $this->getParameter('app.github_pass');
		$ghUrl = $this->getParameter('app.github_url');
		$httpClient = HttpClient::create(['auth_basic' => ['maciekstary', 'muc83!git']]);
		$response = $httpClient->request('GET',
				$ghUrl.'/repos/'.$ownerAndRepo.'/pulls',
				['query' => ['state' => $state]]);

		$statusCode = $response->getStatusCode();
		if ($statusCode !== 200) {
			return $this->json('Something went wrong on GitHub server.', 500);
		};
		$contentArr = $response->toArray();		
		
		$linkHeader = null;
		if (isset($response->getHeaders()['link'])) {
			$linkHeader = $response->getHeaders()['link'][0];
			$lastPageResponse = $httpClient->request('GET', $this->getLastPageUrl($linkHeader));
			$lastPageStatusCode = $lastPageResponse->getStatusCode();
			if ($lastPageStatusCode !== 200) {
				//return $this->json('Something went wrong on GitHub server.', 500);
				//throw new \Exception('Something went wrong on GitHub server.');
				return null;
			}
			$lastPageContentArr = $lastPageResponse->toArray();
			$lastPageSize = count($lastPageContentArr);	
			$pullsCount = $this->getLastPageNumber($linkHeader) * 30 - (30 - $lastPageSize);
		} else {
			$pullsCount = count($contentArr);
		}
	
		return $pullsCount;
	}
	/**
	 * 
	 * @param string $ownerAndRepo
	 * @return float
	 */
	private function getAverageWeeklyCommitCount(string $ownerAndRepo): float {
		$ghUser = $this->getParameter('app.github_user');
		$ghPass = $this->getParameter('app.github_pass');
		$ghUrl = $this->getParameter('app.github_url');
		$httpClient = HttpClient::create(['auth_basic' => ['maciekstary', 'muc83!git']]);
		$response = $httpClient->request('GET',
				$ghUrl.'/repos/'.$ownerAndRepo.'/stats/participation');

		$statusCode = $response->getStatusCode();
		
		if ($statusCode !== 200) {
			return $this->json('Something went wrong on GitHub server.', 500);
		};
		$contentArr = $response->toArray();	
		
		$allWeeksCount = $contentArr['all'];
		$avgCount = array_sum($allWeeksCount)/count($allWeeksCount);
		
		return round($avgCount, 1);
	}

	/**
	 * 
	 * @param string $linkHeader
	 * @return int
	 */
	private function getLastPageNumber(string $linkHeader): int {
		$lastPosStart = strrpos($linkHeader, 'page=') + strlen('page=');
		$lastPosEnd = strrpos($linkHeader, '>');
		$lastPageNumber = substr($linkHeader, $lastPosStart, $lastPosEnd - $lastPosStart);
		
		return (int)$lastPageNumber;
	}
	/**
	 * 
	 * @param string $linkHeader
	 * @return string
	 */
	private function getLastPageUrl(string $linkHeader): string {
		$urlPosStart = strrpos($linkHeader, '<') + 1;
		$urlPosEnd = strrpos($linkHeader, '>');
		$lastPageUrl = substr($linkHeader, $urlPosStart, $urlPosEnd - $urlPosStart);
		
		return $lastPageUrl;
	}
	/**
	 * @Route("/")
	 * @return type
	 */
	public function welcomePage() {
		return Response::create('Welcome to Repo Stats app.', Response::HTTP_OK); 
	}
}
